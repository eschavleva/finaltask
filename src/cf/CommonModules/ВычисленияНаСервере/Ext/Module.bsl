﻿Процедура Расчет(НаборДвижений, ВидРасчета, Сотрудники) Экспорт
	
	Регистратор = НаборДвижений.Отбор.Регистратор.Значение;
	Если ВидРасчета = ПланыВидовРасчета.ОсновныеНачисления.Оклад Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	НачисленияСотрудниковДанныеГрафика.НомерСтроки,
		               |	НачисленияСотрудниковДанныеГрафика.ЗначениеФактическийПериодДействия,
		               |	ДанныеСотрудниковСрезПоследних.Оклад,
		               |	НачисленияСотрудниковДанныеГрафика.ЗначениеПериодДействия
		               |ИЗ
		               |	РегистрРасчета.НачисленияСотрудников.ДанныеГрафика(
		               |			ВидРасчета = &ВидРасчета
		               |				И Регистратор = &Регистратор
		               |				И Сотрудник В (&Сотрудники)) КАК НачисленияСотрудниковДанныеГрафика
		               |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ РегистрСведений.ДанныеСотрудников.СрезПоследних(&ПериодРегистрации, ) КАК ДанныеСотрудниковСрезПоследних
		               |		ПО НачисленияСотрудниковДанныеГрафика.Сотрудник = ДанныеСотрудниковСрезПоследних.Сотрудник";
		
		Запрос.УстановитьПараметр("ВидРасчета", ВидРасчета);
		Запрос.УстановитьПараметр("ПериодРегистрации", Регистратор.Период);
		Запрос.УстановитьПараметр("Регистратор", Регистратор);
		Запрос.УстановитьПараметр("Сотрудники", Сотрудники);
		
		Результат = Запрос.Выполнить();
		Выборка = Результат.Выбрать();
		СтруктураПоиска = Новый Структура("НомерСтроки");
		
		Для Каждого СтрокаНабора Из НаборДвижений Цикл
			Если СтрокаНабора.ВидРасчета <> ВидРасчета Тогда
				Продолжить;
			КонецЕсли;
			СтруктураПоиска.НомерСтроки = СтрокаНабора.НомерСтроки;
			Выборка.Сбросить();
			Если Выборка.НайтиСледующий(СтруктураПоиска) Тогда
				СтрокаНабора.Результат = Выборка.Оклад * Выборка.ЗначениеФактическийПериодДействия / Выборка.ЗначениеПериодДействия;
			КонецЕсли;
		КонецЦикла;
	ИначеЕсли ВидРасчета = ПланыВидовРасчета.ДополнительныеНачисления.Премия Тогда
		Для Каждого СтрокаНабора Из НаборДвижений Цикл
			СтрокаНабора.Результат = Константы.СуммаПремии.Получить();
		КонецЦикла;
	КонецЕсли;
	НаборДвижений.Записать(, Истина);		
	
КонецПроцедуры